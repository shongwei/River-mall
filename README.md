# River mall

#### 介绍
river Mall 商城系统源码，包括后台，数据库，app源码，小程序、管理后台页面源码

#### 软件架构
sptingboot  ，redis，mybits，uniapp，mysql


#### 使用说明

1.core文件夹下面是公用的一些包
db文件夹下面是连接数据库的相关文件
admin-api 文件夹是后台接口
wx-api 是前台调用的接口列表

admin是后台管理页面源码

app 是客户端源码，安卓  ios 小程序 通用

采用的后台采用的springboot框架，数据库使用mysql

APP做了国际化支持，包括 中文，印尼语，英语，其他语言自行修改

没时间写过多的注释，代码里面包括部分注释，放部分截图上来，能用的上的自行研究
![APP小程序首页](https://images.gitee.com/uploads/images/2020/0424/210630_4de3e2c3_4769838.png "app1.png")
![分类页面](https://images.gitee.com/uploads/images/2020/0424/210702_84be8c6d_4769838.png "APP2.png")
![购物车页面](https://images.gitee.com/uploads/images/2020/0424/210720_6ef8cce8_4769838.png "app3.png")

![个人页面](https://images.gitee.com/uploads/images/2020/0424/210739_f6edc307_4769838.png "app4.png")

![管理后天页面](https://images.gitee.com/uploads/images/2020/0424/210758_dc26d72a_4769838.png "h1.png")
![管理后天页面](https://images.gitee.com/uploads/images/2020/0424/210817_4f0b3467_4769838.png "h2.png")


在传几张国际化的
![输入图片说明](https://images.gitee.com/uploads/images/2020/0424/210519_19bfb1cf_4769838.png "y1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0424/210531_f0b234bf_4769838.png "y2.png")
