import Vue from 'vue'
import store from './store'
import App from './App'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n);
Vue.config.productionTip = false


const i18n = new VueI18n({
	//locale: localStorage.getItem('locale') || 'zh-CN', //初始化,保证刷新页面也保留
	//locale:'id',
	locale: 'zh-CN',
	messages: { 
		'zh-CN': require('./locales/zh-CN.js').lang,//中文
		'en': require('./locales/en.js').lang,//英文
		'id':require('./locales/id.js').lang,//印尼语
	}
})
Vue.prototype._i18n = i18n

Vue.prototype.$i18nMsg = function(){
    return i18n.messages[i18n.locale]
} 


import Json from './Json' //测试用数据
/**
 *  因工具函数属于公司资产, 所以直接在Vue实例挂载几个常用的函数
 *  所有测试用数据均存放于根目录json.js
 *  
 *  css部分使用了App.vue下的全局样式和iconfont图标，有需要图标库的可以留言。
 *  示例使用了uni.scss下的变量, 除变量外已尽量移除特有语法,可直接替换为其他预处理器使用
 */
// const i18n = new VueI18n({  
//   locale: 'zh-CN',  // 默认选择的语言
//   messages 
// })
//Vue.prototype.apiRoot = 'http://localhost:8082/wx/'
const msg = (title, duration=1500, mask=false, icon='none')=>{
	//统一提示方便全局修改
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}
Vue.prototype.shareListConst = [{
		type: 1,
		icon: '/static/temp/share_wechat.png',
		text: '微信好友'
	},
	{
		type: 2,
		icon: '/static/temp/share_moment.png',
		text: '朋友圈'
	},
	{
		type: 3,
		icon: '/static/temp/share_qq.png',
		text: 'QQ好友'
	},
	{
		type: 4,
		icon: '/static/temp/share_qqzone.png',
		text: 'QQ空间'
	}
]
const json = type=>{
	//模拟异步请求数据
	return new Promise(resolve=>{
		setTimeout(()=>{
			resolve(Json[type]);
		}, 500)
	})
}

const prePage = ()=>{
	let pages = getCurrentPages();
	let prePage = pages[pages.length - 2];
	// #ifdef H5
	return prePage;
	// #endif
	return prePage.$vm;
}


Vue.config.productionTip = false
Vue.prototype.$fire = new Vue();
Vue.prototype.$store = store;
Vue.prototype.$api = {msg, json, prePage};

App.mpType = 'app'
// Vue.prototype.$i18n = i18n

const app = new Vue({
	i18n,
    ...App
})
app.$mount()