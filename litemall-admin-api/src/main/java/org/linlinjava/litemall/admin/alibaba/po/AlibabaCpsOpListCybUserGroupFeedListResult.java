package org.linlinjava.litemall.admin.alibaba.po;

import java.util.Date;

public class AlibabaCpsOpListCybUserGroupFeedListResult {

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 商品id
     */
    private Long feedId;
    /**
     * 商品标题
     */
    private String title;
    /**
     * 原则
     */
    private String price;
    /**超买价*/
    // private  String promotionPrice;
    /**优惠空间*/
    // private  String promotionSpace;
    /**
     * 是否失效，有效false;无效true，查看invalidInfo字段失效原因
     */
    private Boolean invalid;
    /**失效信息*/
    // private  String  invalidInfo;
    /**
     * 商品首图
     */
    private String imgUrl;

    /**
     * 销量
     */
    //  private String saleCount;
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setFeedId(Long feedId) {
        this.feedId = feedId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public void setInvalid(Boolean invalid) {
        this.invalid = invalid;
    }


    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public Long getFeedId() {
        return feedId;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }


    public Boolean getInvalid() {
        return invalid;
    }


    public String getImgUrl() {
        return imgUrl;
    }
}

