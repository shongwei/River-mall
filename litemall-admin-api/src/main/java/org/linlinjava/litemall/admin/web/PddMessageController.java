//package org.linlinjava.litemall.admin.web;
//
//
//import com.alibaba.fastjson.JSON;
//import com.pdd.pop.sdk.message.MessageHandler;
//import com.pdd.pop.sdk.message.WsClient;
//import com.pdd.pop.sdk.message.model.Message;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.linlinjava.litemall.admin.config.PddConfig;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
//
//@Component
//public class PddMessageController   implements ApplicationRunner {
//    private final Log logger = LogFactory.getLog(PddMessageController.class);
//
//
//
//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//
//        WsClient ws = new WsClient(
//                "wss://message-api.pinduoduo.com", PddConfig.CILIENT_ID,
//                PddConfig.CLIENT_SECRET, new MessageHandler() {
//            @Override
//            public void onMessage(Message message) throws InterruptedException {
//                //业务处理
//                logger.info("获取到拼多多 订阅消息:::"+ JSON.toJSONString(message));
//            }
//        });
//        ws.connect();
//    }
//
//
//
//}
