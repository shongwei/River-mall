package org.linlinjava.litemall.admin.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsInfosBuyerViewParam;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsInfosBuyerViewResult;
import com.alibaba.ocean.rawsdk.ApiExecutor;
import com.alibaba.ocean.rawsdk.common.SDKResult;
import com.alibaba.product.param.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.linlinjava.litemall.admin.alibaba.AlibabaCpsMediaProductInfoParam;
import org.linlinjava.litemall.admin.alibaba.AlibabaCpsOpListCybUserGroupFeedParam;
import org.linlinjava.litemall.admin.alibaba.AlibabaCpsOpListCybUserGroupParam;
import org.linlinjava.litemall.admin.alibaba.AlibabaCpsOpListCybUserGroupResult;
import org.linlinjava.litemall.admin.alibaba.vo.AlibabaMessageGoodsSkuVo;
import org.linlinjava.litemall.core.config.GlobalConfig;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallGoodsProduct;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;
import org.linlinjava.litemall.db.service.LitemallOrderGoodsService;
import org.linlinjava.litemall.db.service.LitemallOrderService;
import org.linlinjava.litemall.db.util.OrderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;


@Service
public class AlibabaService {
    private final Log logger = LogFactory.getLog(AlibabaService.class);

    @Autowired
    private LitemallOrderService litemallOrderService;
    @Autowired
    private AlibabaGoodsChangeService alibabaGoodsChangeService;
    @Autowired
    private LitemallOrderGoodsService litemallOrderGoodsService;

    public void skuMessageChange(AlibabaMessageGoodsSkuVo vo){

        if(alibabaGoodsChangeService.skuMessageChangeResult(vo)){
            logger.info("此选品未同步,取消关注,OfferId:::"+vo.getOfferId());
            this.productUnFollow(Long.valueOf(vo.getOfferId()));
        }
    }

    /**
     * 关注商品：
     * **/

    public Boolean productFollow(Long productId) {
        Boolean flag = false;
        AlibabaProductFollowParam param = new AlibabaProductFollowParam();
        logger.info("关注商品");
         param.setProductId(productId);
        SDKResult<AlibabaProductFollowResult> result =  apiExecutor().execute(param);
        if(result.getResult().getMessage().equals("success"))
            flag = true;
        return flag;
    }
    /**取消关注*/
    public Boolean productUnFollow(Long productId) {
        Boolean flag = false;
        AlibabaProductUnfollowCrossborderParam param = new AlibabaProductUnfollowCrossborderParam();
        logger.info("取消关注");
         param.setProductId(productId);
        SDKResult<AlibabaProductUnfollowCrossborderResult> result =  apiExecutor().execute(param);
        if(result.getResult().getMessage().equals("success"))
            flag=true;
        return flag;
    }

    /**
     *
     *
     * */
    public int getProductInfo(Long offerId,Float franking,Boolean selfPrice,Long categoryId) {
        int flag = 0;
        AlibabaCpsMediaProductInfoParam paramProduct = new AlibabaCpsMediaProductInfoParam();
        paramProduct.setOfferId(offerId);
        paramProduct.setNeedCpsSuggestPrice(true);
        SDKResult<AlibabaCpsMediaProductInfoResult> resultInfo = apiExecutor().execute(paramProduct);
        //logger.info(JSON.toJSON(resultInfo));
        if(resultInfo!=null&&resultInfo.getResult()!=null&&resultInfo.getResult().getSuccess()){
            //获取商品信息成功
            logger.info("获取商品信息成功");
            //商品信息
            /**
             *
             * 根据此处获取的商品信息生成对应的数据库信息，必须获取的三个参数(下单必填参数)
             * offerId,商品ID
             * specId：sku编码
             * qualityLevel：质量等级
             *
             * 其他获取参数:
             * 成本价格，
             * 需要手动维护的数据，详情内容 和邮费  以及销售价格
             * */
            flag=alibabaGoodsChangeService.alibabaGoodsOrmGoods(resultInfo.getResult().getProductInfo(),franking,selfPrice,categoryId);
        }else{
            logger.info("获取商品信息失败, 失败原因:"+resultInfo.getErrorMessage());
        }
        return flag;
    }

    /**
     *
     * 获取选品库列表
     * */
    public SDKResult<AlibabaCpsOpListCybUserGroupResult> listCybUserGroup(String pageNo,String pageSize){
        AlibabaCpsOpListCybUserGroupParam param = new AlibabaCpsOpListCybUserGroupParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
       return apiExecutor().execute(param);

    }
    /**
     *
     * 根据选品库Id获取商品列表
     * */
    public Object listCybUserGroupFeed(Long groupId,Integer pageNo ,Integer pageSize){
        AlibabaCpsOpListCybUserGroupFeedParam param = new AlibabaCpsOpListCybUserGroupFeedParam();
        param.setGroupId(groupId);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        return apiExecutor().execute(param).getResult();

    }

    /**
     * 根据订单编号查询物流详情
     * */
    public void getLogisticsInfos(String orderId){
        AlibabaTradeGetLogisticsInfosBuyerViewParam param = new AlibabaTradeGetLogisticsInfosBuyerViewParam();
        param.setOrderId(Long.parseLong(orderId));
        param.setWebSite("1688");

        SDKResult<AlibabaTradeGetLogisticsInfosBuyerViewResult> result = apiExecutor().execute(param);
        if(result.getResult().getSuccess()){

            LitemallOrder order = new LitemallOrder();
            order.setShipChannel(result.getResult().getResult()[0].getLogisticsCompanyName());
            order.setShipSn(result.getResult().getResult()[0].getLogisticsBillNo());
            order.setShipTime(LocalDateTime.now());
            order.setAlibabaOrderid(orderId);
            order.setOrderStatus(OrderUtil.STATUS_SHIP);
            litemallOrderService.updateWithAlibabaOrderId(order);

            LitemallOrderGoods orderGoods = new LitemallOrderGoods();
            orderGoods.setAlibabaOrderid(orderId);
            orderGoods.setShipChannel(result.getResult().getResult()[0].getLogisticsCompanyName());
            orderGoods.setShipSn(result.getResult().getResult()[0].getLogisticsBillNo());
            orderGoods.setShipTime(LocalDateTime.now());
            litemallOrderGoodsService.updateByAlibabaOrderId(orderGoods);

        }
    }

    private ApiExecutor apiExecutor(){
        //获取配置信息appid和appsecret
        ApiExecutor apiExecutor = new ApiExecutor(GlobalConfig.AlibabaAppId,GlobalConfig.AlibabaAppSecret);
        return apiExecutor;
    }




}
