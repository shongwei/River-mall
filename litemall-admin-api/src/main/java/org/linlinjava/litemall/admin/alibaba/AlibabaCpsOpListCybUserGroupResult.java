package org.linlinjava.litemall.admin.alibaba;

import java.util.*;
import java.math.BigDecimal;
import java.math.BigInteger;

public class AlibabaCpsOpListCybUserGroupResult {

    private Integer totalRow;
    private Object result;

    public void setTotalRow(Integer totalRow) {
        this.totalRow = totalRow;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Integer getTotalRow() {
        return totalRow;
    }

    public Object getResult() {
        return result;
    }
}
