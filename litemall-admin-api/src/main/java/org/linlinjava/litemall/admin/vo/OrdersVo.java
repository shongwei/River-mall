package org.linlinjava.litemall.admin.vo;

import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class OrdersVo {
    private Integer id;
    private Integer userId;
    private String orderSn;
    private Short orderStatus;
    private String consignee;
    private String mobile;
    private String address;
    private String message;
    private BigDecimal goodsPrice;
    private BigDecimal freightPrice;
    private BigDecimal couponPrice;
    private BigDecimal integralPrice;
    private BigDecimal grouponPrice;
    private BigDecimal orderPrice;
    private BigDecimal actualPrice;
    private String payId;
    private LocalDateTime payTime;
    private String shipSn;
    private String shipChannel;
    private LocalDateTime shipTime;
    private LocalDateTime confirmTime;
    private Short comments;
    private LocalDateTime endTime;
    private LocalDateTime addTime;
    private LocalDateTime updateTime;
    private String alibabaOrderid;
    private Integer purchasingStatus;
    private String purchasingFailReason;
    private Long purchasingPrice;
    private String level;
    private List<OrdersVo> children;

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public void setOrderStatus(Short orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public void setFreightPrice(BigDecimal freightPrice) {
        this.freightPrice = freightPrice;
    }

    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    public void setIntegralPrice(BigDecimal integralPrice) {
        this.integralPrice = integralPrice;
    }

    public void setGrouponPrice(BigDecimal grouponPrice) {
        this.grouponPrice = grouponPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    public void setPayId(String payId) {
        this.payId = payId;
    }

    public void setPayTime(LocalDateTime payTime) {
        this.payTime = payTime;
    }

    public void setShipSn(String shipSn) {
        this.shipSn = shipSn;
    }

    public void setShipChannel(String shipChannel) {
        this.shipChannel = shipChannel;
    }

    public void setShipTime(LocalDateTime shipTime) {
        this.shipTime = shipTime;
    }

    public void setConfirmTime(LocalDateTime confirmTime) {
        this.confirmTime = confirmTime;
    }

    public void setComments(Short comments) {
        this.comments = comments;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public void setAlibabaOrderid(String alibabaOrderid) {
        this.alibabaOrderid = alibabaOrderid;
    }

    public void setPurchasingStatus(Integer purchasingStatus) {
        this.purchasingStatus = purchasingStatus;
    }

    public void setPurchasingFailReason(String purchasingFailReason) {
        this.purchasingFailReason = purchasingFailReason;
    }

    public void setPurchasingPrice(Long purchasingPrice) {
        this.purchasingPrice = purchasingPrice;
    }

    public void setChildren(List<OrdersVo> children) {
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public Short getOrderStatus() {
        return orderStatus;
    }

    public String getConsignee() {
        return consignee;
    }

    public String getMobile() {
        return mobile;
    }

    public String getAddress() {
        return address;
    }

    public String getMessage() {
        return message;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public BigDecimal getFreightPrice() {
        return freightPrice;
    }

    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    public BigDecimal getIntegralPrice() {
        return integralPrice;
    }

    public BigDecimal getGrouponPrice() {
        return grouponPrice;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public BigDecimal getActualPrice() {
        return actualPrice;
    }

    public String getPayId() {
        return payId;
    }

    public LocalDateTime getPayTime() {
        return payTime;
    }

    public String getShipSn() {
        return shipSn;
    }

    public String getShipChannel() {
        return shipChannel;
    }

    public LocalDateTime getShipTime() {
        return shipTime;
    }

    public LocalDateTime getConfirmTime() {
        return confirmTime;
    }

    public Short getComments() {
        return comments;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public String getAlibabaOrderid() {
        return alibabaOrderid;
    }

    public Integer getPurchasingStatus() {
        return purchasingStatus;
    }

    public String getPurchasingFailReason() {
        return purchasingFailReason;
    }

    public Long getPurchasingPrice() {
        return purchasingPrice;
    }

    public List<OrdersVo> getChildren() {
        return children;
    }
}
