package org.linlinjava.litemall.admin.alibaba;

import com.alibaba.ocean.rawsdk.client.APIId;
import com.alibaba.ocean.rawsdk.common.AbstractAPIRequest;
import com.alibaba.product.param.AlibabaCpsMediaProductInfoResult;

import java.util.*;
import java.math.BigDecimal;
import java.math.BigInteger;

public class AlibabaCpsMediaProductInfoParam extends AbstractAPIRequest<AlibabaCpsMediaProductInfoResult> {

    public AlibabaCpsMediaProductInfoParam() {
        super();
        oceanApiId = new APIId("com.alibaba.product", "alibaba.cpsMedia.productInfo", 1);
    }

    private Long offerId;
    private Boolean needCpsSuggestPrice;
    /**
     * @return 1688商品ID，等同于productId
     */
    public Long getOfferId() {
        return offerId;
    }

    /**
     * 设置1688商品ID，等同于productId     *
     * 参数示例：<pre>573741401425</pre>     
     * 此参数必填
     */
    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public void setNeedCpsSuggestPrice(Boolean needCpsSuggestPrice) {
        this.needCpsSuggestPrice = needCpsSuggestPrice;
    }

    public Boolean getNeedCpsSuggestPrice() {
        return needCpsSuggestPrice;
    }
}
