package org.linlinjava.litemall.admin.vo;

import org.linlinjava.litemall.db.domain.Products;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ProductsVo {
    private String productslist;
    private Long categoryId;

    public void setProductslist(String productslist) {
        this.productslist = productslist;
    }

    public String getProductslist() {
        return productslist;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }



    public Long getCategoryId() {
        return categoryId;
    }
}
