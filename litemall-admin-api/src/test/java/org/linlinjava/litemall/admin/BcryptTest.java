package org.linlinjava.litemall.admin;

import com.alibaba.ocean.rawsdk.ApiExecutor;
import com.alibaba.product.param.AlibabaProductFollowParam;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.linlinjava.litemall.core.util.bcrypt.BCryptPasswordEncoder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class BcryptTest {

    @Test
    public void test() {
        String rawPassword = "aaaaaa";
        String encodedPassword = "";
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        encodedPassword = bCryptPasswordEncoder.encode(rawPassword);

        System.out.println("rawPassword=" + rawPassword + " encodedPassword=" + encodedPassword);

        Assert.assertTrue(bCryptPasswordEncoder.matches(rawPassword, encodedPassword));
    }
    public static  void main(String[] args){
//        ApiExecutor apiExecutor = new ApiExecutor("2794560", "aYyuPwfCbi3");
//        AlibabaProductFollowParam param = new AlibabaProductFollowParam();
//        Long productId = Long.parseLong("600559711625");
//        param.setProductId(productId);
//        System.out.println(apiExecutor.execute(param));

            String[] arr = {"棕色","38"};
            String[] brr = {"38","棕色"};
            System.out.println(arr.equals(brr));
          //  String[]
       // System.out.println( Arrays.sort(arr));



    }
}
