package org.linlinjava.litemall.core.vo;

import lombok.Data;

@Data
public class Lock {

	private String name;
	private String value;

	public Lock(String name, String value) {
		this.name = name;
		this.value = value;
	}
}