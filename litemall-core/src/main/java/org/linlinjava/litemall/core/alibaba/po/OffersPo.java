/**  
 * <p>Title: Image.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月15日  
 */  
package org.linlinjava.litemall.core.alibaba.po;

import lombok.Data;

/**  
 * <p>Title: Image</p>  
 * <p>Description: </p>  
 * @author yica
 * @date 2019年6月15日  
 */
@Data
public class OffersPo {
	private Long id;
	private String specId;
	private Long price; //媒体溢价
	private  Integer num;

	public OffersPo(){

	}
	public void setId(Long id) {
		this.id = id;
	}

	public void setSpecId(String specId) {
		this.specId = specId;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Long getId() {
		return id;
	}

	public String getSpecId() {
		return specId;
	}

	public Long getPrice() {
		return price;
	}

	public Integer getNum() {
		return num;
	}
}
