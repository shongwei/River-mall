package org.linlinjava.litemall.core.config;

import org.springframework.stereotype.Component;


@Component(value = "GlobalConfig")
public class GlobalConfig {

//	public final static String wxAppId="wx1c505e190d4390cc";
	public final static String AlibabaAppId = "";
	public final static String AlibabaAppSecret = "";

	public final static  String messageSkuType = "PRODUCT_PRODUCT_INVENTORY_CHANGE";//库存变更
	public final static  String SENDGOODS ="ORDER_BUYER_VIEW_ANNOUNCE_SENDGOODS";//订单发货
	public final static String part_sendGoods ="ORDER_BUYER_VIEW_PART_PART_SENDGOODS";//商品部分发货
	public final static String comfirm_receivegoods="ORDER_BUYER_VIEW_ORDER_COMFIRM_RECEIVEGOODS";//订单确认收货
	public final static String order_success = "ORDER_BUYER_VIEW_ORDER_SUCCESS";//交易成功交易成功


	public final static String ORDER_BUYER_CLOSE = "ORDER_BUYER_VIEW_ORDER_BUYER_CLOSE";//买家关闭订单
	public final static String REFUND_IN_SALES = "ORDER_BUYER_VIEW_ORDER_BUYER_REFUND_IN_SALES";//订单售中退款
	public final static String ORDER_REFUND_AFTER_SALES = "ORDER_BUYER_VIEW_ORDER_REFUND_AFTER_SALES";//售后退款


	public final static String PRODUCT_DELETE = "PRODUCT_RELATION_VIEW_PRODUCT_DELETE";//产品删除 productIds
	public final static String PRODUCT_EXPIRE = "PRODUCT_RELATION_VIEW_PRODUCT_EXPIRE";//产品下架productIds
	public final static String PRODUCT_AUDIT = "PRODUCT_RELATION_VIEW_PRODUCT_AUDIT";//产品审核下架productIds

	public final static String SUPER_BUY_PRICE="PRODUCT_RELATION_VIEW_EXIT_SUPERBUYER";//超买价变更通知
	public final static String BUYER_PAYED_SUCCEED="CAIGOU_MSG_BUYER_PAYED_SUCCEED";//付款成功消息订阅

	public final static String[] ArrtibutesArr = {"流行元素",
													"颜色",
													"尺码",
													"流行元素",
													"鞋头形状",
													"货源类别",
													"产地",
													"风格",
													"适用",
													"性别",
													"适合季节",
													"最快出货时间",
													"品牌",
													"内里材质",
													"鞋帮高度",
													"鞋跟形状",
													"鞋底工艺",
													"穿着方式",
													"是否外贸",
													"是否库存",
													"适用运动",
													"鞋垫材质",
													"质检报告",
													"质检单位",
													"适用场景",
													"上市年份",
													"最晚发货时间",
													"是",
													"否跨境货源",
													"适用年龄段",
													"开口深度",
													"上市年份季节（上市时间）",
													"是否跨境货源",
													"主要销售地区",
													"主要下游平台",
													"适用性别",

													"有可授权的自有品牌",
													"库存类型",

													"功能",
													"闭合方式",
													"产品类别",
													"适用人群类型",
													"包装体积",
													"外贸类型",
													"加工方式",
													"系列",
													"形象",
			"上市年份季节","场景","适用送礼场合","图案","加工定制","加印LOGO","有无防雨罩","包内部结构","是否支持分销","加盟分销门槛","货号","型号","加工定制","小包内部结构","商家编码",
	"适合身高","上市年份/季节","工艺","淘货类别","主面料成分2","主面料成分2的含量","主面料成分3","主面料成分3的含量","主面料产地是否进口","适合年龄","风格类型","主图来源","工艺","误差范围","款号",
	"是否支持贴牌","是否支持代工","是否进口","适用人群类别","设计特色","改版","最低贴牌量","生产周期","贴牌对象","主要销售渠道","适合人群","服装口袋样式","款式细节","是否套装",
	"领口形状","代理类型","上新类型","货源类型","门襟","领型"};



}