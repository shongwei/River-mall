package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallRedPacaketsMapper;
import org.linlinjava.litemall.db.dao.StatMapper;
import org.linlinjava.litemall.db.domain.LitemallRedPacakets;
import org.linlinjava.litemall.db.domain.LitemallRedPacaketsExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class LitemallRedPacaketsService {
    @Resource
    private LitemallRedPacaketsMapper redPacaketsMapper;

    private String message;
    private String enterprisePaymentMessage;

    public String getEnterprisePaymentMessage() {
        return enterprisePaymentMessage;
    }

    public void setEnterprisePaymentMessage(String enterprisePaymentMessage) {
        this.enterprisePaymentMessage = enterprisePaymentMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<LitemallRedPacakets> getlist(Integer limit ,Boolean homeshow){
        LitemallRedPacaketsExample redPacakets = new LitemallRedPacaketsExample();
       // redPacakets.or().andStatusEqualTo(1).andEndTimeGreaterThan(LocalDateTime.now()).andItemNumGreaterThan(0);
       // LitemallRedPacaketsExample.Criteria criteria = redPacakets.or();
        if(homeshow) {
            // criteria.andHomeShowEqualTo(1);
            redPacakets.or().andStatusEqualTo(1).andEndTimeGreaterThan(LocalDateTime.now()).andItemNumGreaterThan(0).andHomeShowEqualTo(1);
        }else {
            redPacakets.or().andStatusEqualTo(1).andEndTimeGreaterThan(LocalDateTime.now()).andItemNumGreaterThan(0);
        }
        if (limit!=null&&limit>0){
            PageHelper.offsetPage(1,limit);
        }

        return redPacaketsMapper.selectByExampleSelective(redPacakets);
    }
    public LitemallRedPacakets findById(Long id){
        return redPacaketsMapper.selectByPrimaryKey(id);
    }
    public  void updatePlatformCutpriceItemNum(Long id,int itemNum){
        LitemallRedPacakets pacakets = new LitemallRedPacakets();
        pacakets = redPacaketsMapper.selectByPrimaryKey(id);
        pacakets.setItemNum(itemNum);
        redPacaketsMapper.updateByPrimaryKeySelective(pacakets);

    }
    public void updatePlatformCutpriceTop(Long id ,int top){
        LitemallRedPacakets pacakets = new LitemallRedPacakets();
        pacakets = redPacaketsMapper.selectByPrimaryKey(id);
        pacakets.setTop(top);
        redPacaketsMapper.updateByPrimaryKey(pacakets);
    }
    public int updateRedPacakets(LitemallRedPacakets pacakets){
        return redPacaketsMapper.updateByPrimaryKey(pacakets);
    }
}
