package org.linlinjava.litemall.db.po;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ProjectName: diesa_server
 * @Package: com.tmxk.diesa.po
 * @ClassName: UserParticipant
 * @Author: Administrator
 * @Description: ${description}
 * @Date: 2019/7/23 0023 13:17
 * @Version:
 */
@Data
@NoArgsConstructor
public class FriendCutPacketPo implements Serializable {
  private static final long serialVersionUID = -1992533903886793220L;

  private String userId;//
  private Long userCutpriceId;//id
}
