package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;

import org.linlinjava.litemall.db.dao.ReceiveRecordMapper;
import org.linlinjava.litemall.db.domain.ReceiveRecord;
import org.linlinjava.litemall.db.domain.ReceiveRecordExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReceiveRecordService {
    @Resource
    private ReceiveRecordMapper receiveRecordMapper;

   public List<ReceiveRecord> findreceiveRecordByListIdAndstatus(long barginListId,int status){
       ReceiveRecordExample example = new ReceiveRecordExample();
       example.or().andBargainListIdEqualTo(barginListId).andStatusEqualTo(status);
       return receiveRecordMapper.selectByExample(example);
   }
   public int getSelfStatus(Long bargainId){
       ReceiveRecordExample example = new ReceiveRecordExample();
       example.or().andStatusEqualTo(1).andBargainListIdEqualTo(bargainId);
       return  (int)receiveRecordMapper.countByExample(example);
   }
   public void savereceive(List<ReceiveRecord> re){
       for(ReceiveRecord record:re){
           receiveRecordMapper.insert(record);
       }

   }
   public  ReceiveRecord findReceiveRecordByBargainByBargainListIdAndOpenid(Long historyId,String userId){
       ReceiveRecordExample example = new ReceiveRecordExample();
       example.or().andBargainListIdEqualTo(historyId).andOpenidEqualTo(userId);
       return  receiveRecordMapper.selectOneByExample(example);
   }
   public void updateReceiveRecordStatus(Long id, int status, String thadeNo){
       ReceiveRecord receiveRecord = new ReceiveRecord();
       receiveRecord.setId(id);
       receiveRecord.setStatus(status);
       receiveRecord.setGainTime(LocalDateTime.now());
       receiveRecord.setTradeNo(thadeNo);
       receiveRecordMapper.updateByPrimaryKeySelective(receiveRecord);
   }
   public void insertByRecord(ReceiveRecord receiveRecord){
       receiveRecordMapper.insert(receiveRecord);
   }
}
