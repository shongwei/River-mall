package org.linlinjava.litemall.db.po;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @Description: 对应mng_coupon表的实体类
 * @Auther: wangly
 * @Date: 2019/4/18 0018 10:54
 */
@Entity
@Table(name = "mng_coupon")
@Data
@NoArgsConstructor
public class MngCoupon implements Serializable {
  private static final long serialVersionUID = -2810756112349872493L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(unique = true, nullable = false)
  private Long id;

  private String title;//优惠券标题

  private String icon;//logo图片
  @Column(name = "coupon_category")
  private Integer couponCategory;//类型：1.单品券 2. 品类代金券 3.定额满减券 4.随机立减券 5.折扣券 6.全场代金券

  private int used ;//可用于：10店铺优惠券 11新人店铺券  20商品优惠券  30类目优惠券  60平台优惠券 61新人平台券

  @Column(name = "with_special")
  private int withSpecial;//1可用于特价商品 0不能  默认不能  (是否可以叠加使用)
  @Column(name = "with_sn")
  private String withSn;//商品优惠券为foodid，类目优惠券为分类id，店铺优惠券为storeid
  @Column(name = "with_amount")
  private Long withAmount;//满多少金额
  @Column(name = "used_amount")
  private Long usedAmount ;//抵用金额，或折扣额(1-99)%"

  private Long minAmount ;//随机立减券最小随机金额
  private Long totalDiscounts ;//优惠总额限制

  @Column(name = "person_quota")
  private int personQuota =1;//个人可领取优惠券数量，默认1张
  @Column(name = "all_quota")
  private Integer allQuota ;//配额：发券数量限额：发券总数量，为0不可再领取。
  @Column(name = "take_count")
  private int takeCount;//已领取的优惠券数量
  @Column(name = "used_count")
  private int usedCount;//已使用的优惠券数量
  @Column(name = "start_time")
  private Date startTime;//发放开始时间
  @Column(name = "end_time")
  private Date endTime;//发放结束时间
  @Column(name = "valid_start_time")
  private Date validStartTime;//使用开始时间
  @Column(name = "valid_end_time")
  private Date validEndTime;//使用结束时间
  @Column(name = "create_time",updatable = false)
  private Date createTime;
  @Column(name = "valid_type")
  private int validType;//时效:1绝对时效（领取后XXX-XXX时间段有效）  2相对时效（领取后N天有效）
  @Column(name = "valid_days")
  private int validDays ;//自领取之日起有效天数

  private int status;//1生效 2失效 3已结束
  @Column(name = "store_id",updatable = false)
  private Long storeId;//商户ID，平台自身为0
  
  private Integer usageMode;//使用方式：0 通用 1到店使用 2外卖专用
  
  private Integer  useLimit;//一次可同时使用的优惠券张数
  

  private String descr;//优惠券说明
}
