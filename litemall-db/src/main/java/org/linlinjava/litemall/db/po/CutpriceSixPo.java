package org.linlinjava.litemall.db.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Auther: wangly
 * @Date: 2019/5/28 0028 09:29
 */

@Data
@NoArgsConstructor
public class CutpriceSixPo implements Serializable {
  private static final long serialVersionUID = -28107234529872493L;

    private  Long id;

    private String batchId;//微信代金券标识

    private String title;//活动标题

    private String image;//自定义活动图片
    private String image2;//自定义活动图片
    private String image3;//自定义活动图片
    private String url;//自定义活动图片url
    private String url2;//自定义活动图片url2
    private String url3;//自定义活动图片url3
    private String content;//活动详情说明

    private int target;//砍价目标，折扣百分比%

    private int status;//活动状态：1激活 2失效

    private int cutpriceMode;//砍价模式，1砍到目标价才能购买，2随时购买且终止砍价

    private int duration;//活动持续时间，单位秒

    private Date startTime;//

    private Date endTime;

    private Date createTime;

    private String subtitleSponser; //发起人看到的副标题

    private String subtitleParticipant;//参与者看到的副标题

    private int selfAvailable;//自己参与砍价：0不可以，1可以

    private int price;//原价
    private  int auction;
    private Integer top;
    private int itemsQuota;
    private int itemNum;
    private Integer parties;

    private int minTimes;//最少砍价次数
    private int maxTimes;//最大砍价次数
    private int firstReduce;//第一刀占总额的百分比

    private Integer labelUrgent;
    private int labelNew;
    private int labelEasy;
    private Integer deadline;//红包领取时间限制，秒数,0不限制
    private Integer selfReceivable;//0发起人不可领红包1可领
    private Integer selfPercent;//发起人固定红包百分比
    private Integer display;//1显示0不显示
    private Integer advertisingViews;//广告播放次数门限，剩余最小砍价次数小于此值时，每次砍价强制用户看广告
    private Integer receivableNum;//可领取红包的人数，0为所有参与人可领

    private Long activityListId;//cutprice id，设定的活动id
    private int needParties;//红包需要参与人数
    private int gainableNum;//可参与领取人数
    // private int mode;//砍价模式，1砍到目标价才能购买，2随时购买且终止砍价3普通红包4随机领取红包5竞速领取红包6定时发放红包
    private int activityListStatus;//活动状态：1激活 2失效
    private Date activityListCreateTime;
    private List<String> userIdsCanDo;
  private Date finishTime;
  private int selfStatus;
  private int  bannerAdvertType ;//广告的类型，0图片，1视频
  private String  advertisingVideo ;
}
