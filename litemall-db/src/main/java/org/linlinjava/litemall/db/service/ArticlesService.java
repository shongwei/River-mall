package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.dao.ArticlesMapper;
import org.linlinjava.litemall.db.dao.SysConfigMapper;
import org.linlinjava.litemall.db.domain.Articles;
import org.linlinjava.litemall.db.domain.ArticlesExample;
import org.linlinjava.litemall.db.domain.SysConfig;
import org.linlinjava.litemall.db.domain.SysConfigExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ArticlesService {
    @Resource
    private ArticlesMapper articlesMapper;

    public Articles findOne(Long id){

        return articlesMapper.selectByPrimaryKey(id);
    }


}
