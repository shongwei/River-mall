package org.linlinjava.litemall.db.po;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @Auther: yica
 * @Date: 2019/5/28 0028 09:29
 */

@Data
@NoArgsConstructor
public class MngUserCutpricePo implements Serializable {
  private static final long serialVersionUID = -28107234529872493L;

  private Long id;
  private Long cutpriceId;//砍价活动id

  @Column(name = "store_id",updatable = false)
  private Long storeId;
  private Long couponId;//参与砍价者获赠的优惠券ID
  private String userId;//用户openid
  private Long orderId;//订单ID
  private int parties;//目前参与人数
  private Date createTime;//发起砍价活动时间
  private int status;//活动状态：1进行中 2成功 3失效 4已支付消费
  private int needCutvalue;//需要砍减的目标金额
  private int totalReducation;//目前总减价额

  private int price;//原价
  private int cutpriceMode;//砍价模式，1砍到目标价才能购买，2随时购买且终止砍价

  private int min;//可变的最小砍价金额；
  private int minPerreduce;//单次最小砍价金额
  private int maxPerreduce;//单次最大砍价金额


  private int duration;//活动持续时间，单位秒
  private String subtitleParticipant;//参与者看到的副标题   助力好友，赢取店铺优惠券！
  private String image;//商家自定义活动图片

  private int selfAvailable;
  
  private  Integer auction;
  
  private String title;

  private int minTimes;//最少砍价次数
  private int maxTimes;//最大砍价次数
  private int firstReduce;//第一刀占总额的百分比
  private Integer advertisingViews;//广告播放次数门限，剩余最小砍价次数小于此值时，每次砍价强制用户看广告
  private String nickName;//发起人昵称
  private String avatarUrl;//发起人头像

  public void setId(Long id) {
    this.id = id;
  }

  public void setCutpriceId(Long cutpriceId) {
    this.cutpriceId = cutpriceId;
  }

  public void setStoreId(Long storeId) {
    this.storeId = storeId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public void setParties(int parties) {
    this.parties = parties;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public void setNeedCutvalue(int needCutvalue) {
    this.needCutvalue = needCutvalue;
  }

  public void setTotalReducation(int totalReducation) {
    this.totalReducation = totalReducation;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public void setCutpriceMode(int cutpriceMode) {
    this.cutpriceMode = cutpriceMode;
  }

  public void setMin(int min) {
    this.min = min;
  }

  public void setMinPerreduce(int minPerreduce) {
    this.minPerreduce = minPerreduce;
  }

  public void setMaxPerreduce(int maxPerreduce) {
    this.maxPerreduce = maxPerreduce;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public void setSubtitleParticipant(String subtitleParticipant) {
    this.subtitleParticipant = subtitleParticipant;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public void setSelfAvailable(int selfAvailable) {
    this.selfAvailable = selfAvailable;
  }

  public void setAuction(Integer auction) {
    this.auction = auction;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setMinTimes(int minTimes) {
    this.minTimes = minTimes;
  }

  public void setMaxTimes(int maxTimes) {
    this.maxTimes = maxTimes;
  }

  public void setFirstReduce(int firstReduce) {
    this.firstReduce = firstReduce;
  }

  public void setAdvertisingViews(Integer advertisingViews) {
    this.advertisingViews = advertisingViews;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public Long getId() {
    return id;
  }

  public Long getCutpriceId() {
    return cutpriceId;
  }

  public Long getStoreId() {
    return storeId;
  }

  public Long getCouponId() {
    return couponId;
  }

  public String getUserId() {
    return userId;
  }

  public Long getOrderId() {
    return orderId;
  }

  public int getParties() {
    return parties;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public int getStatus() {
    return status;
  }

  public int getNeedCutvalue() {
    return needCutvalue;
  }

  public int getTotalReducation() {
    return totalReducation;
  }

  public int getPrice() {
    return price;
  }

  public int getCutpriceMode() {
    return cutpriceMode;
  }

  public int getMin() {
    return min;
  }

  public int getMinPerreduce() {
    return minPerreduce;
  }

  public int getMaxPerreduce() {
    return maxPerreduce;
  }

  public int getDuration() {
    return duration;
  }

  public String getSubtitleParticipant() {
    return subtitleParticipant;
  }

  public String getImage() {
    return image;
  }

  public int getSelfAvailable() {
    return selfAvailable;
  }

  public Integer getAuction() {
    return auction;
  }

  public String getTitle() {
    return title;
  }

  public int getMinTimes() {
    return minTimes;
  }

  public int getMaxTimes() {
    return maxTimes;
  }

  public int getFirstReduce() {
    return firstReduce;
  }

  public Integer getAdvertisingViews() {
    return advertisingViews;
  }

  public String getNickName() {
    return nickName;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }
}
