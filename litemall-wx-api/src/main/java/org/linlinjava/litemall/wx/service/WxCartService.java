package org.linlinjava.litemall.wx.service;


import com.alibaba.ocean.rawsdk.common.SDKResult;
import com.alibaba.trade.param.AlibabaCreateOrderPreview4CybMediaParam;
import com.alibaba.trade.param.AlibabaCreateOrderPreview4CybMediaResult;
import com.alibaba.trade.param.AlibabaTradeFastAddress;
import com.alibaba.trade.param.AlibabaTradeFastCargo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.alibaba.AlibabaOrderService;
import org.linlinjava.litemall.db.domain.LitemallAddress;
import org.linlinjava.litemall.db.domain.LitemallCart;
import org.linlinjava.litemall.db.domain.LitemallGoodsProduct;
import org.linlinjava.litemall.db.service.LitemallGoodsProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




@Service
public class WxCartService {
    private final Log logger = LogFactory.getLog(WxCartService.class);
    @Autowired
    private AlibabaOrderService alibabaOrderService;
    @Autowired
    private LitemallGoodsProductService litemallGoodsProductService;

    public SDKResult<AlibabaCreateOrderPreview4CybMediaResult> orderPreview(LitemallAddress address, LitemallCart cart){
        AlibabaTradeFastAddress addressParam = new AlibabaTradeFastAddress();
        if (address != null) {
            addressParam.setFullName(address.getName());
            addressParam.setMobile(address.getTel());
            addressParam.setPhone(address.getTel());
            addressParam.setPostCode(alibabaOrderService.getAddreddCode(address.getProvince() + address.getCity() + address.getCounty() + address.getAddressDetail()));
            addressParam.setCityText(address.getCity());
            addressParam.setProvinceText(address.getProvince());
            addressParam.setAreaText(address.getCounty());
            addressParam.setTownText(address.getAddressDetail());
            addressParam.setAddress(address.getProvince() + address.getCity() + address.getCounty() + address.getAddressDetail());
            addressParam.setDistrictCode(address.getAreaCode());
            ///接口中的地址id不知如何设置
            // addressParam.setAddressId(Long.valueOf(address.getId()));


        }
        LitemallGoodsProduct product = litemallGoodsProductService.findById(cart.getProductId());
        //:alibaba.createOrder.preview4CybMedia-1

        AlibabaCreateOrderPreview4CybMediaParam param = new AlibabaCreateOrderPreview4CybMediaParam();
        AlibabaTradeFastCargo[] cargos = new AlibabaTradeFastCargo[1];
        if(product!=null&&product.getAlibabaProductid()!=null){
            AlibabaTradeFastCargo cargo = new AlibabaTradeFastCargo();
            cargo.setOfferId(product.getAlibabaProductid());
            cargo.setQuantity(cart.getNumber().doubleValue());
            if(product.getAlibabaSpecid()!=null)
                cargo.setSpecId(product.getAlibabaSpecid());
            cargos[0]=cargo;
        }else {
            return null;
        }

        return alibabaOrderService.orderPreview(addressParam,cargos);

    }





}