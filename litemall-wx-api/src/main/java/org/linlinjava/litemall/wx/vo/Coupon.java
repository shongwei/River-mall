/**  
 * <p>Title: WeixinRedPacket.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月13日  
 */  
package org.linlinjava.litemall.wx.vo;

import lombok.Data;

/**  
 * <p>Title: WeixinRedPacket</p>  
 * <p>Description: </p>  
 * @author caiy  
 * @date 2019年6月13日  
 */
@Data
public class Coupon {
	private String coupon_stock_id;
	private String openid_count;
	private String partner_trade_no;
	private String openid;
	private String appid;
	private String mch_id;
	private String device_info;
	private String nonce_str;
	private String sign;

	public void setCoupon_stock_id(String coupon_stock_id) {
		this.coupon_stock_id = coupon_stock_id;
	}

	public void setOpenid_count(String openid_count) {
		this.openid_count = openid_count;
	}

	public void setPartner_trade_no(String partner_trade_no) {
		this.partner_trade_no = partner_trade_no;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getCoupon_stock_id() {
		return coupon_stock_id;
	}

	public String getOpenid_count() {
		return openid_count;
	}

	public String getPartner_trade_no() {
		return partner_trade_no;
	}

	public String getOpenid() {
		return openid;
	}

	public String getAppid() {
		return appid;
	}

	public String getMch_id() {
		return mch_id;
	}

	public String getDevice_info() {
		return device_info;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public String getSign() {
		return sign;
	}
}
