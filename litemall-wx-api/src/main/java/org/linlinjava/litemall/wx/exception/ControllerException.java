package org.linlinjava.litemall.wx.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class ControllerException extends Exception {

  private static final long serialVersionUID = -446375198350395568L;
  private HttpStatus httpStatus;

  public ControllerException(HttpStatus httpStatus, String message) {
    super(message);
    this.httpStatus = httpStatus;
  }
}
