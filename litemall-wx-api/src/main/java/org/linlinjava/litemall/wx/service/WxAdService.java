package org.linlinjava.litemall.wx.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallAd;
import org.linlinjava.litemall.db.service.LitemallAdService;
import org.linlinjava.litemall.wx.po.AdDanmuListPo;
import org.linlinjava.litemall.wx.po.AdPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class WxAdService {
    private final Log logger = LogFactory.getLog(WxAdService.class);

    @Autowired
    private LitemallAdService adService;

    public AdPo list() {
        LitemallAd ad  = adService.getRandLitemallAd();
        //LitemallAd ad  = adService.queryIndex();
        AdPo adPo = new AdPo();
        if (ad!=null) {

            adPo.setName(ad.getName());
            adPo.setLink(ad.getLink());
            adPo.setUrl(ad.getUrl());
            List<AdDanmuListPo> danmuList = new ArrayList<>();
            AdDanmuListPo po = new AdDanmuListPo();
            po.setText(ad.getContent());
            po.setTime(1);
            po.setColor("#f07890");
            danmuList.add(po);
            adPo.setDanmuList(danmuList);
        }
            return adPo;
    }






}