/**  
 * <p>Title: WeixinRedPacket.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月13日  
 */  
package org.linlinjava.litemall.wx.vo.wechart;

import lombok.Data;

/**  
 * <p>Title: WeixinRedPacket</p>  
 * <p>Description: </p>  
 * @author caiy  
 * @date 2019年6月13日  
 */
@Data
public class EnterprisePayment {
	private String mch_appid;
	private String mchid;
	private String nonce_str;
	private String partner_trade_no;	
	private String openid;
	private String check_name;
	private String amount;
	private String desc;	
	private String spbill_create_ip;
	private String sign;//签名

	public void setMch_appid(String mch_appid) {
		this.mch_appid = mch_appid;
	}

	public void setMchid(String mchid) {
		this.mchid = mchid;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public void setPartner_trade_no(String partner_trade_no) {
		this.partner_trade_no = partner_trade_no;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public void setCheck_name(String check_name) {
		this.check_name = check_name;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getMch_appid() {
		return mch_appid;
	}

	public String getMchid() {
		return mchid;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public String getPartner_trade_no() {
		return partner_trade_no;
	}

	public String getOpenid() {
		return openid;
	}

	public String getCheck_name() {
		return check_name;
	}

	public String getAmount() {
		return amount;
	}

	public String getDesc() {
		return desc;
	}

	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	public String getSign() {
		return sign;
	}
}
