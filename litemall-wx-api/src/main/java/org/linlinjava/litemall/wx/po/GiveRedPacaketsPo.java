/**  
 * <p>Title: Image.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月15日  
 */  
package org.linlinjava.litemall.wx.po;

import lombok.Data;

/**  
 * <p>Title: Image</p>  
 * <p>Description: </p>  
 * @author yica
 * @date 2019年6月15日  
 */
@Data
public class GiveRedPacaketsPo {
	private Long userItemListId;
	private String appletName;
	private String openId;

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setUserItemListId(Long userItemListId) {
		this.userItemListId = userItemListId;
	}

	public void setAppletName(String appletName) {
		this.appletName = appletName;
	}

	public Long getUserItemListId() {
		return userItemListId;
	}

	public String getAppletName() {
		return appletName;
	}
}
