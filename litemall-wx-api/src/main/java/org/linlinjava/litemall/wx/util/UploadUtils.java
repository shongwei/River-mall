package org.linlinjava.litemall.wx.util;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * @ProjectName: diesa_server
 * @Package: com.tmxk.diesa.utils
 * @ClassName: UploadUtils
 * @Author: Administrator
 * @Description: ${description}
 * @Date: 2019/7/16 0016 14:10
 * @Version:
 */

public class UploadUtils {
  // 项目根路径下的目录  -- SpringBoot static 目录相当于是根路径下（SpringBoot 默认）
  public final static String IMG_PATH_PREFIX = "static/upload/imgs";

  public static File getImgDirFile(){

    // 构建上传文件的存放 "文件夹" 路径
    String fileDirPath = new String("src/main/resources/" + IMG_PATH_PREFIX);

    File fileDir = new File(fileDirPath);
    if(!fileDir.exists()){
      // 递归生成文件夹
      fileDir.mkdirs();
    }
    return fileDir;
  }

  //byte数组到图片到硬盘上
  public static void byte2image(byte[] data,String path) {
    if(data.length<3||path.equals("")) return;//判断输入的byte是否为空
    try{
      FileImageOutputStream imageOutput = new FileImageOutputStream(new File(path));//打开输入流
      imageOutput.write(data, 0, data.length);//将byte写入硬盘
      imageOutput.close();
     // log.info("Make Picture success,Please find image in " + path);
    } catch(Exception ex) {
     //log.error(ex.getMessage() , ex);
    }
  }
}
